public class Client{
    public static void main(String[] args){
        Dengen dengen;
        dengen = new AcAdapter();
        int denatsu = dengen.kyuuden();
        System.out.println(denatsu+"V で給電されています");
    }
}

class Dengen{
    public int kyuuden();
}

class JapaneseConsent{
    private int power = 100;
    public int getPower(){
        return power;
    }
}

class AcAdapter implements Dengen{
    private JapaneseConsent consent = new JapaneseConsent();
    public int kyuuden(){
        return (int)(consent.getPower()*0.16);
    }
}