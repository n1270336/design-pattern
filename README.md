# design-pattern

## 0610

[SingletonTest](SingletonTest)

## 0624

[IteratorTest](IteratorTest)

## 0708

[TheDogTest1](TheDogTest1)
![TheDogTest1](TheDogTest1/TheDogTest1.png)

## 0722

[FactoryMethod](FactoryMethod)

## 0729

[ObserverTest](ObserverTest)

## 0902

[Observer](Observer)

## 0916

[Adapter](Adapter)