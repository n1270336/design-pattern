public class SingletonTest{
    public static void main(String[] args){
        Renban renbanA = Renban.getInstance();
        Renban renbanB = Renban.getInstance();

        try{
            System.out.println(renbanA.getNumber());
            System.out.println(renbanB.getNumber());
            System.out.println(renbanB.getNumber());
            System.out.println(renbanA.getNumber());
            System.out.println(renbanB.getNumber());
        }catch(RangeException e){
            System.out.println(e);
        }
    }
}

class Renban{
    private static Renban renban = new Renban();
    private int serial_number;

    private Renban(){
        this.serial_number = 0;
    }

    public static Renban getInstance(){
        return renban;
    }

    public String getNumber() throws RangeException{
        if(this.serial_number<0||this.serial_number>9999){
            throw new RangeException("serial_number is out of range");
        }
        return String.format("%04d", this.serial_number++);
    }
}

class RangeException extends Exception{
    public RangeException(){
        super();
    }
    public RangeException(String message){
        super(message);
    }
}
